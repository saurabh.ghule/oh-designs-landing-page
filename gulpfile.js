//require('es6-promise').polyfill();
var theme_path="./";
var vendorjs_path=theme_path+'assets/js/vendors/';
var nm=theme_path+'node_modules/';
var gulp = require('gulp');
var colors = require('colors');
var sass = require('gulp-sass');
var is_local_environment = process.env.NODE_ENV !== "production";
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var minify = require('gulp-minify-css');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");

function starify(message){
    var stars= '********************************************************   '.rainbow;
    console.log(stars +' ' + message + ' ' + stars);
}
function showMessages(){
    starify( ("Message from".white + ' Saurabh '.green.bold).bgRed );
    var message = "You are running in: "+  (is_local_environment ? 'local mode'.green.bold : 'production mode'.red.bold  ) ;
    starify( message  );

    if(is_local_environment){
        var general = "Don't forget to run".red + " 'npm run prod' ".blue.bold + " before pushing the code".red;
        starify( general.bgWhite  );
    }
}
showMessages();

gulp.task('sass', function () {

    gulp.src(theme_path+'assets/sass/app.scss')
        .pipe( gulpif(is_local_environment,sourcemaps.init()) )
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(plumber())
        .pipe(concat('app.css'))
        .pipe( gulpif(!is_local_environment,minify()) )
        .pipe( gulpif(is_local_environment,sourcemaps.write()) )
        .pipe(gulp.dest(theme_path+'/public/css/'))

});
gulp.task('js', function () {

    gulp.src(theme_path+'assets/js/custom/**/*.js')
        .pipe(plumber())
        .pipe( gulpif(is_local_environment,sourcemaps.init()) )
        .pipe(concat('app.js'))
        .pipe( gulpif(!is_local_environment,uglify()) )
        .pipe( gulpif(is_local_environment,sourcemaps.write()) )
        .pipe(gulp.dest(theme_path+'/public/js/'));

});
gulp.task('vendorjs', function () {

    gulp.src(
        [
            vendorjs_path+'jquery-3.4.1.min.js',
            vendorjs_path+'bootstrap.min.js',
            vendorjs_path+'feather.min.js',
            vendorjs_path+'slick.min.js',
        ]
    )
        .pipe(plumber())
        .pipe( gulpif(is_local_environment,sourcemaps.init()) )
        .pipe(concat('vendors.js'))
        .pipe( gulpif(!is_local_environment,uglify()) )
        .pipe( gulpif(is_local_environment,sourcemaps.write()) )
        .pipe(gulp.dest(theme_path+'/public/js/'));

});

gulp.task('default', function () {
    gulp.src(theme_path+'/public/css/app.css')
        .pipe(autoprefixer());
});

gulp.task('watch', function () {
    gulp.watch(theme_path+'/assets/sass/**/*.scss', ['sass']);
    gulp.watch(theme_path+'/assets/js/custom/**/*.js', ['js']);
});

gulp.task('default', ['sass', 'js','vendorjs', 'watch']);